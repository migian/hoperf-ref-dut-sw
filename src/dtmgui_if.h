#include <Arduino.h>

// DTM protocol
#define NUM_CMDS     4
#define DTM_RESET    0x00
#define DTM_RX_TEST  0x01
#define DTM_TX_TEST  0x02
#define DTM_TEST_END 0x03

#define STATUS_MSG   0x00
#define PKT_REP_MSG  0x01

#define STATUS_OK    0x00
#define STATUS_NOK   0x01

#define NUM_CHAN     8
#define BASE_FREQ    867.1F
#define CHAN_SPACING 0.2F

#define NUM_MODULATIONS 6
#define DTM_MOD_OOK     0x00
#define DTM_MOD_LORA    0x01
#define DTM_MOD_FSK     0x02

#define PAYLOAD_PRBS9    0x94
#define PAYLOAD_00001111 0x95
#define PAYLOAD_10101010 0x96
#define PAYLOAD_VENDOR   0x97


#define NUM_DATARATES 7
struct lora_dr_conf {
    uint8_t sf;
    float bw;
};

typedef struct tx_tst_cmd_s {
    uint8_t modulation;
    uint8_t chan_idx;
    uint8_t power;
    uint8_t datarate;
    uint8_t codingrate;
    uint8_t payload;
    uint8_t pl_size;
    uint8_t interval;
    uint8_t pkt_cnt;  // 0 = continuous transmission
} tx_tst_cmd;

typedef struct rx_tst_cmd_s {
    uint8_t empty;
    uint8_t chan_idx;
    uint8_t datarate;
} rx_tst_cmd;

typedef struct status_msg_s {
    uint8_t msg_type;
    uint8_t empty;
    uint8_t status;
} status_msg;

typedef struct packet_report_msg_s {
    uint8_t msg_type;
    uint8_t ok_pkt_cnt;
    uint8_t crc_err_cnt;
    uint8_t rssi_hi;
    uint8_t rssi_lo;
} packet_report_msg;


float idx2freq(uint8_t idx);
uint8_t parse_command(uint8_t *buf);
void send_reply(uint8_t *out_buf, uint8_t status, uint8_t ok_pkt_cnt, uint8_t crc_err_cnt, int16_t rssi);
bool val_in_range(uint8_t low, uint8_t val, uint8_t high);
lora_dr_conf get_sf_bw(uint8_t index);