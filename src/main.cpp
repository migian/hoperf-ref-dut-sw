
#include <RadioLib.h>
#include "dtmgui_if.h"

 // NSS = A3, DIO0 = A2, RST = 8, DIO1 = A1
SX1276 radio = new Module(A3, A2, 8, A1);
// Packet counter for RX test
uint8_t rx_test_ok_pkt_cnt = 0;
uint8_t rx_crc_errors = 0;

#define DIO2_PIN A0
#define RX_READY_PIN A2

// Serial buf
#define SER_BUF_SZ 32
uint8_t serial_buf[SER_BUF_SZ];

#define MAX_TX_PAYLOAD_SIZE 230
uint8_t lora_data_buffer[SX127X_MAX_PACKET_LENGTH];
uint8_t tx_data_size = 0;

enum modes {IDLE = 0, RX = 1, TX_FSK = 2, TX_LORA = 3};
modes mode = IDLE;
uint8_t tx_interval = 0;
uint8_t tx_pkt_cnt = 0;

void read_serial(uint8_t *buf){
  uint8_t *len = buf++; // store len in first byte
  *len = 0;
  
  while(Serial.available()){
    if(*len < SER_BUF_SZ-1)
      *buf++ = Serial.read();
      *len += 1;
  } 
}

void set_dio2(bool on) {
  if(on) {
    pinMode(DIO2_PIN, OUTPUT);
    digitalWrite(DIO2_PIN, 1);
  } else {
    pinMode(DIO2_PIN, INPUT);
  }
}

uint8_t set_fsk_ook_cw_mode(tx_tst_cmd *msg)
{  
  radio.beginFSK();
  radio.setFrequency(idx2freq(msg->chan_idx));
  radio.setBitRate(50.0);
  radio.setFrequencyDeviation(0);
  radio.setRxBandwidth(125.0);
  radio.setOutputPower(msg->power);
  radio.setCurrentLimit(120);

  uint8_t syncWord[] = {0x01, 0x23, 0x45, 0x67,
                        0x89, 0xAB, 0xCD, 0xEF};
  radio.setSyncWord(syncWord, 8);

  if(msg->modulation == DTM_MOD_OOK) {
    radio.setOOK(true);
    radio.setDataShapingOOK(0);
  } else {
    radio.setDataShaping(0.0);
  }
  
  mode = TX_FSK;
  return STATUS_OK;
}

void gen_PRBS9(uint8_t *buf, uint8_t len, bool start_prbs) {
  static uint16_t lfsr;
  int newbit;
  int bytes_written = 0;
  
  if(start_prbs)
    lfsr = 0x69;

  for (int i = 1;; i++) {
    newbit = (((lfsr >> 8) ^ (lfsr >> 4)) & 1);
    lfsr = ((lfsr << 1) | newbit) & 0x1ff;

    if(i % 8 == 0) {
      *buf++ = lfsr;
      if(bytes_written++ == len) {
        break;
      }
    }
  }
}

uint8_t fill_lora_data(uint8_t pld, bool set_pld, uint8_t *buf, uint8_t len, bool start_prbs) {
  static uint8_t payload;
  if(set_pld) 
    payload = pld;

  switch (payload) {
    case PAYLOAD_PRBS9:
      gen_PRBS9(buf, len, start_prbs);
      break;
    case PAYLOAD_00001111:
      memset(buf, B00001111, len);
      break;
    case PAYLOAD_10101010:
      memset(buf, B10101010, len);
      break;
    case PAYLOAD_VENDOR:
      memset(buf, B11111111, len);
      break;
    default:
      return STATUS_NOK;
  }
  return STATUS_OK;
}

uint8_t set_lora_tx_mode(tx_tst_cmd *msg)
{
  lora_dr_conf lora_dr_conf = get_sf_bw(msg->datarate);
  
  radio.begin(idx2freq(msg->chan_idx),   // Frequency
                       lora_dr_conf.bw,  // BW
                       lora_dr_conf.sf,  // SF
                       msg->codingrate,  // CR
                       SX127X_SYNC_WORD, // Sync word
                       msg->power,       // Power
                       120,              // Current limit
                       8,                // Preamble length
                       0);               // Gain
  tx_interval = msg->interval;
  if(tx_interval == 0)
    tx_interval = 1; // Min interval to ensure RX has time to handle packets
  tx_pkt_cnt = msg->pkt_cnt;
  tx_data_size = msg->pl_size;
  if(tx_data_size > MAX_TX_PAYLOAD_SIZE)
    tx_data_size = MAX_TX_PAYLOAD_SIZE;
  
  radio.setCRC(true);
  mode = TX_LORA;
  return STATUS_OK;
}

uint8_t reset_cmd(uint8_t *buf, uint8_t len)
{
  mode = IDLE;
  set_dio2(false);
  radio.reset();
  delay(5);
  return STATUS_OK;
}

uint8_t rx_test_cmd(uint8_t *buf, uint8_t len)
{ 
  if(len != sizeof(rx_tst_cmd))
    return STATUS_NOK;

  rx_tst_cmd *msg = (rx_tst_cmd *) buf; 

  lora_dr_conf lora_dr_conf = get_sf_bw(msg->datarate);

  radio.begin(idx2freq(msg->chan_idx),   // Frequency
                       lora_dr_conf.bw,  // BW
                       lora_dr_conf.sf,  // SF
                       5,                // CR
                       SX127X_SYNC_WORD, // Sync word
                       5,                // Power
                       120,              // Current limit
                       8,                // Preamble length
                       0);               // Gain

  set_dio2(false);

  mode = RX;
  rx_test_ok_pkt_cnt = rx_crc_errors = 0;
  radio.startReceive();

  return STATUS_OK;
}

uint8_t tx_test_cmd(uint8_t *buf, uint8_t len)
{
  uint8_t status = STATUS_OK;
  tx_tst_cmd *msg = (tx_tst_cmd *) buf;
  if(len != sizeof(tx_tst_cmd))
    return STATUS_NOK;

  if(!val_in_range(0, msg->modulation, NUM_MODULATIONS-1))
    return STATUS_NOK;
  
  if(!val_in_range(0, msg->chan_idx, NUM_CHAN-1))
    return STATUS_NOK;
  
  // Modulation
  switch(msg->modulation) {
    case DTM_MOD_OOK:
    case DTM_MOD_FSK:
      return set_fsk_ook_cw_mode(msg);
      break;
    case DTM_MOD_LORA:
      status = set_lora_tx_mode(msg);
      if(status == STATUS_NOK)
        return status;
      return fill_lora_data(msg->payload, true, lora_data_buffer, tx_data_size, true);
      break;
    default:
      return STATUS_NOK; // Other modulations supported yet
  }
  return STATUS_OK;
}

uint8_t test_end_cmd(uint8_t *buf, uint8_t len)
{
  mode = IDLE;
  set_dio2(false);
  radio.standby();
  return STATUS_OK;
}

void setup() {
  Serial.begin(19200);
  mode = IDLE;
  set_dio2(false);
  radio.sleep();
  memset(serial_buf, 0, SER_BUF_SZ);
  tx_data_size = 0;
  memset(lora_data_buffer, 0, SX127X_MAX_PACKET_LENGTH);
  // TODO receiver calibration to EU freq?
  pinMode(A2, INPUT);
  pinMode(A1, INPUT);
}

void loop() {
  // Process commands from DTMGUI
  if(Serial.available()){
     read_serial(serial_buf);
     uint8_t  status = parse_command(serial_buf);
     send_reply(serial_buf, status, rx_test_ok_pkt_cnt, rx_crc_errors, (int16_t) radio.getRSSI());
     rx_test_ok_pkt_cnt = rx_crc_errors = 0;
     memset(serial_buf, 0, SER_BUF_SZ);
  }
  
  // Process RX/TX events
  switch (mode) {
    case RX:
      if(digitalRead(RX_READY_PIN)) {
        int state = radio.readData(lora_data_buffer, SX127X_MAX_PACKET_LENGTH);
        if (state == ERR_NONE) {
          rx_test_ok_pkt_cnt++;
        }
        else if (state == ERR_CRC_MISMATCH) {
          rx_crc_errors++;
        }
        else {
          // some other error occurred
          Serial.print("RX failed, code ");
          Serial.println(state);
        }
        state = radio.startReceive();
      }
      break;
    case TX_LORA:
      fill_lora_data(0, false, lora_data_buffer, tx_data_size, false);
      radio.transmit(lora_data_buffer, tx_data_size);
      delay(tx_interval);
      if(tx_pkt_cnt == 0) {
        // Continuous transmission
      }
      else if(tx_pkt_cnt == 1) {
        mode = IDLE;
        radio.standby();
      }
      else if(--tx_pkt_cnt == 1) {
        fill_lora_data(0, false, lora_data_buffer, tx_data_size, false);
        radio.transmit(lora_data_buffer, tx_data_size);
        mode = IDLE;
        radio.standby();
      }
      break;
    case TX_FSK:
      radio.transmitDirect();
      set_dio2(true); // DIO2 modulates carrier in continuous tx mode     
    default:
      break;
  }
}