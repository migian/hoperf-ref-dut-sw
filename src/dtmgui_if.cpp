#include "dtmgui_if.h"


extern uint8_t reset_cmd(uint8_t *buf, uint8_t len);
extern uint8_t rx_test_cmd(uint8_t *buf, uint8_t len);
extern uint8_t tx_test_cmd(uint8_t *buf, uint8_t len);
extern uint8_t test_end_cmd(uint8_t *buf, uint8_t len);

uint8_t prev_dtm_cmd = DTM_RESET;

const lora_dr_conf lora_dr_conf_list[NUM_DATARATES] = {
  {.sf=12, .bw=125.0F},
  {.sf=11, .bw=125.0F},
  {.sf=10, .bw=125.0F},
  {.sf=9,  .bw=125.0F},
  {.sf=8,  .bw=125.0F},
  {.sf=7,  .bw=125.0F},
  {.sf=7,  .bw=250.0F}
};

lora_dr_conf get_sf_bw(uint8_t index){
  return lora_dr_conf_list[index];
}

bool val_in_range(uint8_t low, uint8_t val, uint8_t high) {
  return (low <= val && val <= high);
}

float idx2freq(uint8_t idx) {
  return (float) (BASE_FREQ+(idx*CHAN_SPACING));
}

uint8_t parse_command(uint8_t *buf){
    
  uint8_t (*cmd_func_ptr[NUM_CMDS])(uint8_t *buf, uint8_t len) = {reset_cmd, rx_test_cmd, tx_test_cmd, test_end_cmd};
  uint8_t status=0;
  
  if(buf[0]==0) {
    Serial.println("Error: parse_command: buf len zero");
    return STATUS_NOK;
  }

  if(val_in_range(0, buf[1], NUM_CMDS-1)) {
    status = (*cmd_func_ptr[buf[1]])(&buf[2], (buf[0]-1));    
    return status;
  }
  else {
    return STATUS_NOK;
  }
}

void send_reply(uint8_t *out_buf, uint8_t status, uint8_t ok_pkt_cnt, uint8_t crc_err_cnt, int16_t rssi){
  if(prev_dtm_cmd == DTM_RX_TEST && out_buf[1] == DTM_TEST_END){
    prev_dtm_cmd = out_buf[1];
    packet_report_msg *msg = (packet_report_msg *) out_buf;
    msg->msg_type = PKT_REP_MSG;
    msg->ok_pkt_cnt = ok_pkt_cnt;
    msg->crc_err_cnt = crc_err_cnt;
    msg->rssi_hi = rssi >> 8;
    msg->rssi_lo = rssi & 0xff;
    Serial.write(out_buf, sizeof(packet_report_msg));
  }
  else {
    prev_dtm_cmd = out_buf[1];
    status_msg *msg = (status_msg *) out_buf;
    msg->msg_type = STATUS_MSG;
    msg->empty = 0;
    msg->status = status;
    Serial.write(out_buf, sizeof(status_msg));
  }
}